from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from .models import Customer, Physician, Service
from .forms import CustForm, TestForm, ServForm


class HomeView(generic.TemplateView):
    login_url = '/login/'
    
    def get_template_names(self, **kwargs):
        if self.request.user.is_authenticated():
            template_name = 'bdnclinic/home2.html'
        else:
            template_name = 'bdnclinic/home.html'
        return template_name
    

class ServIndex(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    model = Service
    template_name = 'bdnclinic/sindex.html'
    context_object_name = 'sl'
    def get_queryset(self):
        return Service.objects.filter(customer__phys__user = self.request.user).order_by('serv_start')

class ServDetail(LoginRequiredMixin, generic.DetailView):
    login_url = '/login/'
    model = Service
    template_name = 'bdnclinic/sdetail.html'
    context_object_name = 's'
    def get_context_data(self, **kwargs):
        context = super(ServDetail,self).get_context_data(**kwargs)
        try:
            context['s'] = Service.objects.get(customer__phys__user = self.request.user, pk = self.kwargs['pk'])
        except Service.DoesNotExist:
            context = None
        return context

class CustIndex(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    model = Customer
    template_name = 'bdnclinic/cindex.html'
    context_object_name = 'cl'
    def get_queryset(self):
        #return Physician.objects.filter(user__username = 'dan' )
        #return Physician.objects.filter(user = self.request.user)
        #return Customer.objects.order_by('lname')
        return Customer.objects.filter(phys__user = self.request.user).order_by('lname')

class CustDetail(LoginRequiredMixin, generic.DetailView):
    login_url = '/login/'
    model = Customer
    template_name = 'bdnclinic/cdetail.html'
    context_object_name = 'c' 
    def get_context_data(self, **kwargs):
        context = super(CustDetail,self).get_context_data(**kwargs) 
        try: 
            context['c'] = Customer.objects.get(phys__user = self.request.user, pk = self.kwargs['pk']) 
        except Customer.DoesNotExist:
            context = None

        return context

class AddView(LoginRequiredMixin, generic.TemplateView):
    login_url = '/login/'
    template_name = 'bdnclinic/add.html'
    
    def get_context_data(self, **kwargs):
        context = super(AddView,self).get_context_data(**kwargs)
        context['cust_form'] = CustForm()
        context['serv_form'] = ServForm()
        context['serv_form'].fields['customer'].queryset = Customer.objects.filter(phys__user=self.request.user)
        context['test_form'] = TestForm()
        return context


'''
@login_required(login_url='/login/', redirect_field_name='/add/')
def add_cust(request):
    cust = request.POST
    physician = Physician.objects.get(user__username=request.user)
    c = Customer(phys=physician, fname=cust['fname'], lname=cust['lname'], dob=cust['dob'])
    c.save()
    messages.success(request, 'Customer added!')
    return HttpResponseRedirect( '/add/')
'''

@login_required(login_url='/login/', redirect_field_name='/add/')
def add_cust(request):
    if request.method == 'POST':
        cust = request.POST
        form = CustForm(cust)
        if form.is_valid:
            physician = Physician.objects.get(user__username=request.user)
            try:
                c = Customer(phys=physician, fname=cust['fname'], lname=cust['lname'], dob=cust['dob'])
                c.save()
            except:
                messages.error(request, 'Error saving customer! Make sure input follows correct format!')
                return HttpResponseRedirect('/add/')

            messages.success(request, 'Customer added!' + request.POST['fname'])
            return HttpResponseRedirect('/add/')
        else:
            messages.error(request, 'Error adding customer! form not valid!')
            return HttpResponseRedirect('/add/')
    else:
        messages.error(request, 'Only accepts POST requests!')
        return HttpResponseRedirect('/add/')


@login_required(login_url='/login/', redirect_field_name='/add/')
def add_serv(request):
    if request.method == 'POST': 
        serv = request.POST
        form = ServForm(serv)
        if form.is_valid:
            phys = Physician.objects.get(user__username=request.user)
            try:
                s = Service(customer=serv['customer'], physician=phys, pay_method=serv['meth'], service_type=serv['stype'], rate=serv['rate'], serv_start=serv['start'], serv_end=serv['end'], billable_hrs=serv['hrs'])
                s.save()
            except:
                messages.error(request, 'Error saving service! Make sure input follows format!')
                return HttpResponseRedirect('/add/')
            messages.success(request, 'Service added!')
            return HttpResponseRedirect('/add/')
        else:
            messages.error(request, 'Error saving server! form not valid!')
            return HttpResponseRedirect('/add/')
    else:
        messages.error(request, 'Only accepts POST request!')
        return HttpResponseRedirect('/add/')

class LogoutView(generic.TemplateView):
    template_name = 'bdnclinic/loggedout.html'


