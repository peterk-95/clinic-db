from django import forms
from django.forms import ModelForm, widgets
from .models import *

#convert to date widget

class CustForm(ModelForm):
    class Meta:
        model = Customer
        fields = [ 'fname', 'lname', 'dob']
        widgets = {
                'dob': forms.DateInput(attrs={'type':'date'}),
                }
        labels = {
                'fname': 'First name',
                'lname': 'Last name',
                'dob': 'DOB',
                }    
        

class ServForm(ModelForm):   
    class Meta:
        model = Service
       
        fields = ['customer', 'pay_method', 'service_type', 'rate', 'serv_start', 'serv_end', 'billable_hrs']
	widgets = {
                'serv_start': forms.DateInput(attrs={'type':'datetime-local'}),
                'serv_end': forms.DateInput(attrs={'type':'datetime-local'}),
                'rate': forms.NumberInput(attrs={'class':'test'}),
		}

    #can use either or to change field
    '''
    def __init__(self, *args, **kwargs):
	super(ServForm, self).__init__(*args, **kwargs)
	self.fields['rate'].widget.attrs.update({'class': 'TEST'})
    '''
        


class TestForm(forms.Form):
    fname = forms.CharField(label='your name', max_length=100)
    lname = forms.CharField(label='your name', max_length=100)
