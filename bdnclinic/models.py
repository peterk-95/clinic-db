from django.db import models
from django.core.validators import MaxValueValidator 
from django.contrib.auth.models import User
import datetime

    

class Physician(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True, blank=True)
    fname = models.CharField(max_length=50)
    lname = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    def __str__(self):
        return self.lname + ', ' + self.fname

class Customer(models.Model):
    phys = models.ForeignKey(Physician, on_delete=models.DO_NOTHING, null=True)
    fname = models.CharField(max_length=50)
    lname = models.CharField(max_length=50)
    dob = models.DateField('date of birth')
    #age = models.IntegerField(default=0)
    def age(self):
        return int((datetime.date.today() - self.dob).days / 365.25)
    age = property(age)
    
    def __str__(self):
        return self.lname + ', ' + self.fname + ': ' + self.phys.fname

class Service(models.Model):
    PAYMENT_CHOICES = (
                ('CC', 'Credit Card'),
                ('DB', 'Debit Card'),
                ('CASH', 'Cash'),
                ('CHQ', 'Cheque'),
                ('ETRNS', 'E-transfer'),
                
            ) 

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    pay_method = models.CharField(max_length=5, choices=PAYMENT_CHOICES)
    service_type = models.CharField(max_length=10)
    rate = models.DecimalField(max_digits=7, decimal_places=2)
    serv_start = models.DateTimeField('service start')
    serv_end = models.DateTimeField('service end')
    billable_hrs = models.PositiveSmallIntegerField(validators=[MaxValueValidator(20)])
    def age_during_service(self):
        return int((self.serv_end.date() - self.customer.dob ).days / 365.25)
    ads = property(age_during_service)
    
    def total(self):
        return float(self.rate) * self.billable_hrs
    amt_due = property(total)
    
    def __str__(self):
        return self.customer.lname + ', ' + self.customer.fname + ': ' + self.service_type
